default:
  @just --list

build:
  cargo build

lint:
  cargo clippy -- -D warnings -D clippy::all -D clippy::pedantic

format:
  cargo clippy --fix -- -D warnings -D clippy::all -D clippy::pedantic

lint-big-times:
  cargo clippy -- -D warnings -D clippy::all -D clippy::pedantic -D clippy::nursery
