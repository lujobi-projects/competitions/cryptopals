#[cfg(test)]
mod tests {
    use crate::utils::{
        conversion::{bytes_to_ascii, hex_to_bytes},
        frequency::frequency,
        xor::fixed_xor,
    };

    #[test]
    fn it_works() {
        let bytes =
            &hex_to_bytes("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736");

        let frequencies = frequency(bytes);

        let most_often = frequencies
            .iter()
            .max_by_key(|&(_, count)| {
                // This is always positive and a rough estimate anyway
                #[allow(clippy::cast_precision_loss)]
                #[allow(clippy::cast_sign_loss)]
                ((*count * 1000.0) as u64)
            })
            .unwrap()
            .0;

        let key = most_often ^ b' ';

        let decrypted = fixed_xor(bytes, &vec![key; bytes.len()]);

        assert_eq!(
            bytes_to_ascii(&decrypted),
            "Cooking MC's like a pound of bacon"
        );
    }
}
