use crate::Byte;

pub fn has_repeating_blocks(ciphertext: &[Byte], block_size: usize) -> bool {
    let mut blocks = Vec::new();

    for block in ciphertext.chunks(block_size) {
        if blocks.contains(&block) {
            return true;
        }

        blocks.push(block);
    }

    false
}

#[cfg(test)]

mod tests {
    use crate::{
        set_01::c_08_detect_aes_in_ecb_mode::has_repeating_blocks,
        utils::{conversion::base64_to_bytes, file_io::read_file},
    };

    #[test]
    fn detect_aes_in_ecb_mode() {
        let file_content = read_file(&std::path::Path::new("data/8.txt"));

        let mut detected = vec![];

        for (id, line) in file_content.lines().enumerate() {
            let ciphertext = base64_to_bytes(line);

            if has_repeating_blocks(&ciphertext, 16) {
                detected.push(id);
            }
        }

        println!("{:?}", detected);

        assert_eq!(detected, vec![132]);
    }
}
