#[cfg(test)]
mod tests {

    use std::path::Path;

    use crate::{
        utils::{
            conversion::{bytes_to_ascii, hex_to_bytes},
            file_io::read_file,
            frequency::english_chi_squared_score,
            xor::fixed_xor,
        },
        Byte,
    };

    #[test]
    fn it_works() {
        let encrypted = read_file(Path::new("data/set_01/c_04.txt"));

        let mut candidates = vec![];

        for (id, by) in encrypted.split_whitespace().enumerate() {
            let chunk = hex_to_bytes(&by);
            for c in 0..Byte::MAX {
                let decrypted = fixed_xor(&chunk, &vec![c; chunk.len()]);
                let score = english_chi_squared_score(&decrypted);
                if score == f64::INFINITY {
                    continue;
                }

                candidates.push((decrypted, score, id));
            }
        }

        assert!(candidates
            .iter()
            .any(|(d, _, _)| { bytes_to_ascii(d).contains("Now that the party is jumping") }));
    }
}
