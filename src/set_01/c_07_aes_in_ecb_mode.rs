#[cfg(test)]

mod tests {
    use crate::utils::conversion::bytes_to_ascii;

    #[test]
    fn decrypt() {
        let key = b"YELLOW SUBMARINE";
        let ciphertext = crate::utils::conversion::base64_to_bytes(
            &crate::utils::file_io::read_file(&std::path::Path::new("data/7.txt"))
                .replace("\n", ""),
        );

        let decrypted = crate::utils::aes::decrypt_aes_128_ecb(&ciphertext, key);

        println!("{}", bytes_to_ascii(&decrypted));

        assert!(bytes_to_ascii(&decrypted).contains("Play that funky music"));
    }
}
