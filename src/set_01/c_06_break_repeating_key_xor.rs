use crate::{
    utils::{
        frequency::english_chi_squared_score_reject_special, hamming::hamming_distance,
        xor::repeating_key_xor,
    },
    Byte,
};

pub fn detect_xor_keysize(ciphertext: &[Byte]) -> Vec<(usize, f64)> {
    let mut keysize_scores = vec![];

    for keysize in 2..=40 {
        let mut distance = 0.0;
        let mut count = 0;

        for chunk in ciphertext.chunks(keysize * 2).take(4) {
            let a = &chunk[..keysize];
            let b = &chunk[keysize..];

            distance += hamming_distance(a, b) as f64 / keysize as f64;
            count += 1;
        }

        distance /= count as f64;

        keysize_scores.push((keysize, distance));
    }

    keysize_scores
}

const SPECIAL_CHAR_REJECT_PERCENTAGE: f64 = 0.1;

pub fn evaluate_key_size(ciphertext: &[Byte], key_size: usize) -> (Vec<Byte>, f64) {
    let mut key = vec![];

    for i in 0..key_size {
        let block = ciphertext
            .iter()
            .skip(i)
            .step_by(key_size)
            .copied()
            .collect::<Vec<Byte>>();
        let (_, k) = (0..=Byte::MAX)
            .map(|c| {
                let decrypted = block.iter().map(|b| b ^ c).collect::<Vec<Byte>>();
                let score = english_chi_squared_score_reject_special(
                    &decrypted,
                    SPECIAL_CHAR_REJECT_PERCENTAGE,
                );
                (score, c)
            })
            .min_by(|(a, _), (b, _)| a.partial_cmp(b).unwrap())
            .unwrap();

        key.push(k);
    }

    let overall_score = english_chi_squared_score_reject_special(
        &repeating_key_xor(&ciphertext, &key),
        SPECIAL_CHAR_REJECT_PERCENTAGE,
    );

    (key, overall_score)
}

#[cfg(test)]

mod tests {
    use std::path::Path;

    use crate::utils::{
        conversion::{base64_to_bytes, bytes_to_ascii},
        file_io::read_file,
        xor::repeating_key_xor,
    };

    use super::*;

    #[test]
    fn decrypt() {
        let encrypted = read_file(Path::new("data/set_01/c_06.txt")).replace("\n", "");
        let ciphertext = base64_to_bytes(&encrypted);

        let mut key_size = detect_xor_keysize(&ciphertext);
        key_size.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

        let best_3_key_sizes = key_size
            .iter()
            .take(3)
            .map(|(k, _)| *k)
            .collect::<Vec<usize>>();

        assert!(best_3_key_sizes.contains(&29));

        let mut key_scores = best_3_key_sizes
            .iter()
            .map(|k| {
                let (key, score) = evaluate_key_size(&ciphertext, *k);
                (key, score)
            })
            .collect::<Vec<_>>();

        key_scores.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

        let (key, _) = key_scores.first().unwrap();
        let decrypted = repeating_key_xor(&ciphertext, key);

        println!("{}", bytes_to_ascii(&decrypted));
        println!("{}", bytes_to_ascii(&key));

        assert_eq!("Terminator X: Bring the noise", bytes_to_ascii(&key));
        assert!(decrypted.starts_with(b"I'm back and I'm ringin' the bell"));
    }
}
