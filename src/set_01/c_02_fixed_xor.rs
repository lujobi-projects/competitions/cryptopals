#[cfg(test)]
mod tests {

    use crate::utils::{
        conversion::{bytes_to_hex, hex_to_bytes},
        xor::fixed_xor,
    };

    #[test]
    fn it_works() {
        let bytes1 = hex_to_bytes("1c0111001f010100061a024b53535009181c");
        let bytes2 = hex_to_bytes("686974207468652062756c6c277320657965");
        let bytes = fixed_xor(&bytes1, &bytes2);
        let hex = bytes_to_hex(&bytes);
        assert_eq!(hex, "746865206b696420646f6e277420706c6179");
    }
}
