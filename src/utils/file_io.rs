use std::path::Path;

pub fn read_file(path: &Path) -> String {
    std::fs::read_to_string(path).unwrap()
}
