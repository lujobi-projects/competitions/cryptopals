use crate::Byte;
use aes::cipher::{block_padding::Pkcs7, BlockDecryptMut, KeyInit};

type Aes128EcbDec = ecb::Decryptor<aes::Aes128>;

pub fn decrypt_aes_128_ecb(ciphertext: &[Byte], key: &[Byte; 16]) -> Vec<Byte> {
    Aes128EcbDec::new(key.into())
        .decrypt_padded_vec_mut::<Pkcs7>(&ciphertext)
        .unwrap()
        .to_vec()
}
