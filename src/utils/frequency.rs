use std::collections::HashMap;

use crate::Byte;

pub fn frequency(bytes: &[Byte]) -> HashMap<Byte, f64> {
    let mut freq = HashMap::new();
    #[allow(clippy::cast_precision_loss)]
    let total = bytes.len() as f64;

    for &byte in bytes {
        *freq.entry(byte).or_insert(0.0) += 1.0;
    }

    for count in freq.values_mut() {
        *count /= total;
    }

    freq
}

// https://www3.nd.edu/~busiforc/handouts/cryptography/letterfrequencies.html
pub const ENGLISH_FREQ: [f64; 26] = [
    0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015, // A-G
    0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749, // H-N
    0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758, // O-U
    0.00978, 0.02360, 0.00150, 0.01974, 0.00074, // V-Z
];

pub fn english_chi_squared_score(bytes: &[Byte]) -> f64 {
    english_chi_squared_score_reject_special(bytes, f64::MAX)
}

pub fn english_chi_squared_score_reject_special(
    bytes: &[Byte],
    reject_special_percentage: f64,
) -> f64 {
    let mut count = [0; 26];
    let mut ignored = 0;
    let mut ignored_without_spaces = 0;

    for &byte in bytes {
        let c = byte;
        if c >= b'A' && c <= b'Z' {
            count[(c - b'A') as usize] += 1;
        } else if c >= b'a' && c <= b'z' {
            count[(c - b'a') as usize] += 1;
        } else if c > b' ' && c <= b'~' {
            ignored += 1;
            ignored_without_spaces += 1;
        } else if c == b'\t' || c == b' ' || c == b'\n' || c == b'\r' {
            ignored += 1;
        } else {
            println!("Ignoring byte: {}", c);
            return f64::INFINITY;
        }
    }

    if (ignored_without_spaces as f64) / bytes.len() as f64 > reject_special_percentage {
        println!("Too many non-ASCII characters");
        return f64::INFINITY;
    }

    let len = (bytes.len() - ignored) as f64;
    let mut chi2 = 0.0;

    for i in 0..26 {
        let observed = count[i] as f64;
        let expected = len * ENGLISH_FREQ[i];
        let difference = observed - expected;
        chi2 += difference * difference / expected;
    }

    chi2
}
