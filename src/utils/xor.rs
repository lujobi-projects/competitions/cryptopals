use crate::Byte;

pub fn fixed_xor(bytes1: &[Byte], bytes2: &[Byte]) -> Vec<Byte> {
    assert_eq!(bytes1.len(), bytes2.len());
    let mut bytes = vec![];
    for (byte1, byte2) in bytes1.iter().zip(bytes2.iter()) {
        bytes.push(byte1 ^ byte2);
    }
    bytes
}

pub fn repeating_key_xor(input: &[u8], key: &[u8]) -> Vec<u8> {
    input
        .iter()
        .zip(key.iter().cycle())
        .map(|(a, b)| a ^ b)
        .collect()
}
