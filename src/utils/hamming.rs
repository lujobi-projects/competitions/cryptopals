use crate::Byte;

pub fn hamming_distance(a: &[Byte], b: &[Byte]) -> u32 {
    assert_eq!(a.len(), b.len());
    a.iter()
        .zip(b.iter())
        .map(|(x, y)| (x ^ y).count_ones())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hamming_distance() {
        let a = b"this is a test";
        let b = b"wokka wokka!!!";

        assert_eq!(hamming_distance(a, b), 37);
    }
}
