use base64::Engine;

use crate::Byte;

pub fn hex_to_bytes(hex: &str) -> Vec<Byte> {
    hex::decode(hex).unwrap()
}

pub fn bytes_to_hex(bytes: &[Byte]) -> String {
    hex::encode(bytes)
}

pub fn bytes_to_base64(bytes: &[Byte]) -> String {
    base64::engine::general_purpose::STANDARD.encode(bytes)
}

pub fn base64_to_bytes(base64: &str) -> Vec<Byte> {
    base64::engine::general_purpose::STANDARD
        .decode(base64)
        .unwrap()
}

pub fn bytes_to_ascii(bytes: &[Byte]) -> String {
    std::str::from_utf8(bytes).unwrap().to_string()
}

pub fn hex_to_base64(hex: &str) -> String {
    let bytes = hex_to_bytes(hex);

    bytes_to_base64(&bytes)
}
